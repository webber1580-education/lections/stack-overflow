﻿namespace StackOverflow.Database.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Content { get; set; }
        
        public int UserId { get; set; }
        public User User { get; set; }

        public int? AnswerId { get; set; }
        public Answer Answer { get; set; }

        public int? QuestionId { get; set; }
        public Question Question { get; set; }
    }
}