﻿using System.Collections.Generic;

namespace StackOverflow.Database.Models
{
    public class Question : BaseEntity
    {
        public string[] Tags { get; set; }
        public string Theme { get; set; }

        public ICollection<Answer> Answers { get; set; }
    }
}