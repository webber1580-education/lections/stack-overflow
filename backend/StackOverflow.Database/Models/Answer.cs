﻿namespace StackOverflow.Database.Models
{
    public class Answer : BaseEntity
    {
        public bool IsCorrect { get; set; }

        public int QuestionId { get; set; }
        public Question Question { get; set; }
    }
}