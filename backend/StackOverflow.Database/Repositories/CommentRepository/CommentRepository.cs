﻿using System;
using System.Threading.Tasks;
using Core.Models.StackOverflow.Dto;
using StackOverflow.Database.Models;

namespace StackOverflow.Database.Repositories.CommentRepository
{
    public class CommentRepository : ICommentRepository
    {
        private readonly StackOverflowDbContext _context;

        public CommentRepository(StackOverflowDbContext context)
        {
            _context = context;
        }
        
        public async Task CreateQuestionCommentAsync(int questionId, CommentDto commentDto)
        {
            await _context.Comments.AddAsync(new Comment()
            {
                Content = commentDto.Content,
                QuestionId = questionId,
                UserId = new Random().Next(1, 4)
            });
            await _context.SaveChangesAsync();
        }

        public async Task CreateAnswerCommentAsync(int answerId, CommentDto commentDto)
        {
            await _context.Comments.AddAsync(new Comment()
            {
                Content = commentDto.Content,
                AnswerId = answerId,
                UserId = new Random().Next(1, 4)
            });
            await _context.SaveChangesAsync();
        }
    }
}