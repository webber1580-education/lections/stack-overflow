﻿using System.Threading.Tasks;
using Core.Models.StackOverflow.Dto;

namespace StackOverflow.Database.Repositories.CommentRepository
{
    public interface ICommentRepository
    {
        Task CreateQuestionCommentAsync(int questionId, CommentDto commentDto);
        Task CreateAnswerCommentAsync(int answerId, CommentDto commentDto);

    }
}