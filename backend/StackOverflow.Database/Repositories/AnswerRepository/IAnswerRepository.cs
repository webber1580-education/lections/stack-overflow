﻿using System.Threading.Tasks;
using Core.Models.StackOverflow.Dto;

namespace StackOverflow.Database.Repositories.AnswerRepository
{
    public interface IAnswerRepository
    {
        Task<AnswerDto> GetAnswerAsync(int answerId);
        Task CreateAnswerAsync(AnswerDto answerDto);
        Task UpdateAnswerAsync(int answerId, AnswerDto answerDto);
    }
}