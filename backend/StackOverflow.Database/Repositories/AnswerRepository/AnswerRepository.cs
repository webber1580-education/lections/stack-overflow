﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Core.Models.StackOverflow.Dto;
using Microsoft.EntityFrameworkCore;
using StackOverflow.Database.Models;

namespace StackOverflow.Database.Repositories.AnswerRepository
{
    public class AnswerRepository : BaseRepository, IAnswerRepository
    {
        public AnswerRepository(StackOverflowDbContext context) : base(context) { }

        public async Task<AnswerDto> GetAnswerAsync(int answerId)
        {
            var answer = await _context.Answers
                .Include(a => a.Comments)
                .Include(a => a.User)
                .FirstOrDefaultAsync(a => a.Id == answerId);
            if (answer == null)
                throw new Exception($"No answer with such id: {answerId}");

            return new AnswerDto()
            {
                Id = answer.Id,
                Content = answer.Content,
                Rating = answer.Rating,
                IsCorrect = answer.IsCorrect,
                Comments = await GetCommentsDtoFromBaseEntity(answer),
                User = GetUserDtoFromUser(answer.User),
            };
        }
        
        public async Task CreateAnswerAsync(AnswerDto answerDto)
        {
            await _context.Answers.AddAsync(new Answer()
            {
                Id = await _context.Answers.CountAsync() + 1,
                Content = answerDto.Content,
                QuestionId = answerDto.QuestionId,
                Rating = answerDto.Rating,
                UserId = new Random().Next(1, 4)
            });
            await _context.SaveChangesAsync();
        }
        
        public async Task UpdateAnswerAsync(int answerId, AnswerDto answerDto)
        {
            var answer = await _context.Answers.FirstOrDefaultAsync(a => a.Id == answerId);
            if (answer == null)
                throw new Exception($"No Answer with such id = {answerId}.");

            answer.Content = answerDto.Content;
            answer.Rating = answerDto.Rating;
            answer.IsCorrect = answerDto.IsCorrect;
            
            _context.Answers.Update(answer);
            await _context.SaveChangesAsync();
        }

    }
}