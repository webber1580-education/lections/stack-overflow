﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Models.StackOverflow.Dto;
using Microsoft.EntityFrameworkCore;
using StackOverflow.Database.Models;

namespace StackOverflow.Database.Repositories.QuestionRepository
{
    public class QuestionRepository : BaseRepository, IQuestionRepository
    {
        public QuestionRepository(StackOverflowDbContext context) : base(context) { }

        public async Task<List<QuestionDto>> GetQuestionsAsync()
        {
            var result = new List<QuestionDto>();
            var questionList = await _context.Questions
                .OrderBy(q => q.Id)
                .Include(q => q.Answers)
                .Include(q => q.Comments)
                .Include(q => q.User)
                .ToListAsync();
            
            foreach (var question in questionList)
            {
                result.Add(new QuestionDto()
                {
                    Id = question.Id,
                    Tags = question.Tags,
                    Content = question.Content,
                    Rating = question.Rating,
                    Theme = question.Theme,
                    Answers = await GetAnswersDtoFromQuestion(question),
                    Comments = await GetCommentsDtoFromBaseEntity(question),
                    User = GetUserDtoFromUser(question.User),
                });
            }

            return result;
        }

        public async Task<QuestionDto> GetQuestionAsync(int questionId)
        {
            var question = await _context.Questions
                .Include(q => q.Answers)
                .Include(q => q.Comments)
                .Include(q => q.User)
                .FirstOrDefaultAsync(q => q.Id == questionId);
            if (question == null)
                throw new Exception($"No question with such id: {questionId}");

            return new QuestionDto()
            {
                Id = question.Id,
                Tags = question.Tags,
                Content = question.Content,
                Rating = question.Rating,
                Theme = question.Theme,
                Answers = await GetAnswersDtoFromQuestion(question),
                Comments = await GetCommentsDtoFromBaseEntity(question),
                User = GetUserDtoFromUser(question.User),
            };
        }

        public async Task CreateQuestionAsync(QuestionDto questionDto)
        {
            await _context.Questions.AddAsync(new Question()
            {
                Id = await _context.Questions.CountAsync() + 1,
                Content = questionDto.Content,
                Rating = questionDto.Rating,
                Tags = questionDto.Tags,
                Theme = questionDto.Theme,
                UserId = new Random().Next(1, 4)
            });
            await _context.SaveChangesAsync();
        }

        public async Task UpdateQuestionAsync(int questionId, QuestionDto questionDto)
        {
            var question = await _context.Questions.FirstOrDefaultAsync(q => q.Id == questionId);
            if (question == null)
                throw new Exception($"No Question with such id = {questionId}.");

            question.Theme = questionDto.Theme;
            question.Content = questionDto.Content;
            question.Rating = questionDto.Rating;
            question.Tags = questionDto.Tags;
            
            _context.Questions.Update(question);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteQuestionAsync(int questionId)
        {
            var question = await _context.Questions.FirstOrDefaultAsync(wv => wv.Id == questionId);
            if (question == null)
                throw new Exception($"No Question with such id = {questionId}.");

            _context.Questions.Remove(question);
            await _context.SaveChangesAsync();

        }

        private async Task<List<AnswerDto>> GetAnswersDtoFromQuestion(Question question)
        {
            List<AnswerDto> result = new List<AnswerDto>();
            var answerList = await _context.Answers
                .Where(a => a.QuestionId == question.Id)
                .OrderBy(a => a.Id)
                .Include(a => a.Comments)
                .Include(a => a.User)
                .ToListAsync();

            foreach (var answer in answerList)
            {
                result.Add(new AnswerDto()
                {
                    Id = answer.Id,
                    Content = answer.Content,
                    Rating = answer.Rating,
                    IsCorrect = answer.IsCorrect,
                    QuestionId = answer.QuestionId,
                    User = GetUserDtoFromUser(answer.User),
                    Comments = await GetCommentsDtoFromBaseEntity(answer)
                });
            }
            return result;
        }
    }
}