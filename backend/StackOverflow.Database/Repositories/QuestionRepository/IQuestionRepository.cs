﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Models.StackOverflow.Dto;

namespace StackOverflow.Database.Repositories.QuestionRepository
{
    public interface IQuestionRepository
    {
        Task<List<QuestionDto>> GetQuestionsAsync();
        Task<QuestionDto> GetQuestionAsync(int questionId);
        Task CreateQuestionAsync(QuestionDto questionDto);
        Task UpdateQuestionAsync(int questionId, QuestionDto questionDto);
        Task DeleteQuestionAsync(int questionId);
    }
}