﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Models.StackOverflow.Dto;
using Microsoft.EntityFrameworkCore;
using StackOverflow.Database.Models;

namespace StackOverflow.Database.Repositories
{
    public class BaseRepository
    {
        protected readonly StackOverflowDbContext _context;

        public BaseRepository(StackOverflowDbContext context)
        {
            _context = context;
        }
        
        protected UserDto GetUserDtoFromUser(User user)
        {
            return new UserDto()
            {
                Id = user.Id,
                Name = user.Name,
                Surname = user.Surname,
                Nickname = user.Nickname,
                Photo = user.Photo
            };
        }

        protected async Task<List<CommentDto>> GetCommentsDtoFromBaseEntity(BaseEntity entity)
        {
            List<Comment> comments;
            if (entity is Answer)
            {
                comments = await _context.Comments
                    .Where(c => c.AnswerId == entity.Id)
                    .OrderBy(c => c.Id)
                    .Include(c => c.User)
                    .ToListAsync();
            }
            else
            {
                comments = await _context.Comments
                    .Where(c => c.QuestionId == entity.Id)
                    .OrderBy(c => c.Id)
                    .Include(c => c.User)
                    .ToListAsync();
            }
            
            return comments
                .OrderBy(c => c.Id)
                .Select(comment => new CommentDto()
                {
                    Id = comment.Id,
                    Content = comment.Content,
                    User = GetUserDtoFromUser(comment.User)
                })
                .ToList();
        }
    }
}