﻿using System.Threading.Tasks;

namespace StackOverflow.Database.Repositories.GeneratorRepository
{
    public interface IGeneratorRepository
    {
        Task GenerateDataAsync();
    }
}