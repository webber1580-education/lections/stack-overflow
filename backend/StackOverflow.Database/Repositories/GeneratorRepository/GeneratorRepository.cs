﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using StackOverflow.Database.Models;

namespace StackOverflow.Database.Repositories.GeneratorRepository
{
    public class GeneratorRepository : IGeneratorRepository
    {
        private const string _base64ImagePrefix = "data:image/png;base64,";
        private readonly StackOverflowDbContext _context;
        private int answersCount = 0;

        
        public GeneratorRepository(StackOverflowDbContext context)
        {
            _context = context;
        }
        
        public async Task GenerateDataAsync()
        {
            await ClearAllDataAsync();
            await GenerateUsersAsync();
            await GenerateQuestionsAsync();
            await _context.SaveChangesAsync();
        }

        private async Task ClearAllDataAsync()
        {
            foreach (var comment in _context.Comments)
                _context.Comments.Remove(comment);
            foreach (var answer in _context.Answers)
                _context.Answers.Remove(answer);
            foreach (var question in _context.Questions)
                _context.Questions.Remove(question);
            foreach (var user in _context.Users)
                _context.Users.Remove(user);

            answersCount = 0;
        }
        
        private async Task GenerateQuestionsAsync()
        {
            var random = new Random();
            
            await _context.Questions.AddAsync(new Question()
            {
                Id = 1,
                Theme = "Отношения",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam quis enim lobortis scelerisque fermentum dui faucibus in. Nunc consequat interdum varius sit amet. Mi eget mauris pharetra et ultrices neque ornare. Interdum consectetur libero id faucibus. Pretium aenean pharetra magna ac. Pulvinar proin gravida hendrerit lectus a. Nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices. Velit ut tortor pretium viverra suspendisse potenti nullam. Lectus mauris ultrices eros in. Neque ornare aenean euismod elementum nisi quis eleifend.",
                UserId = random.Next(1, 4),
                Rating = random.Next(-3, 7),
                Tags = new string[] {"university", "love", "food"}
            });
            await GenerateCommentsToQuestionsAsync(1);
            await GenerateAnswersToQuestionsAsync(1);

            await _context.Questions.AddAsync(new Question()
            {
                Id = 2,
                Theme = "Универ",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                UserId = random.Next(1, 4),
                Rating = random.Next(-3, 7),
                Tags = new string[] {"university", "love", "food"}
            });
            await GenerateCommentsToQuestionsAsync(2);
            await GenerateAnswersToQuestionsAsync(2);

            await _context.Questions.AddAsync(new Question()
            {
                Id = 3,
                Theme = "Python mongoDb exception",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam quis enim lobortis scelerisque fermentum dui faucibus in. Nunc consequat interdum varius sit amet. Mi eget mauris pharetra et ultrices neque ornare. Interdum consectetur libero id faucibus. Pretium aenean pharetra magna ac. Pulvinar proin gravida hendrerit lectus a. Nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices. Velit ut tortor pretium viverra suspendisse potenti nullam. Lectus mauris ultrices eros in. Neque ornare aenean euismod elementum nisi quis eleifend.",
                UserId = random.Next(1, 4),
                Rating = random.Next(-3, 7),
                Tags = new string[] {"university", "love", "food"}
            });
            await GenerateCommentsToQuestionsAsync(3);
            await GenerateAnswersToQuestionsAsync(3);

            await _context.Questions.AddAsync(new Question()
            {
                Id = 4,
                Theme = "Два условно бесконечных цикла",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam quis enim lobortis scelerisque fermentum dui faucibus in. Nunc consequat interdum varius sit amet. Mi eget mauris pharetra et ultrices neque ornare. Interdum consectetur libero id faucibus. Pretium aenean pharetra magna ac. Pulvinar proin gravida hendrerit lectus a. Nisl rhoncus mattis rhoncus urna neque viverra justo nec ultrices. Velit ut tortor pretium viverra suspendisse potenti nullam. Lectus mauris ultrices eros in. Neque ornare aenean euismod elementum nisi quis eleifend.",
                UserId = random.Next(1, 4),
                Rating = random.Next(-3, 7),
                Tags = new string[] {"university", "love", "food"}
            });
            await GenerateCommentsToQuestionsAsync(4);
            await GenerateAnswersToQuestionsAsync(4);

            await _context.Questions.AddAsync(new Question()
            {
                Id = 5,
                Theme = "Модный лук на выпускной",
                Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                UserId = random.Next(1, 4),
                Rating = random.Next(-3, 7),
                Tags = new string[] {"university", "love", "food"}
            });
            await GenerateCommentsToQuestionsAsync(5);
            await GenerateAnswersToQuestionsAsync(5);
        }

        private async Task GenerateCommentsToQuestionsAsync(int questionId)
        {
            var random = new Random();
            var count = random.Next(1, 6);

            for (var i = 1; i < count; i++)
            {
                await _context.Comments.AddAsync(new Comment()
                {
                    UserId = random.Next(1, 4),
                    QuestionId = questionId,
                    Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                });
            }
        }
        
        private async Task GenerateAnswersToQuestionsAsync(int questionId)
        {
            var random = new Random();
            var count = random.Next(1, 6);
            
            for (var i = 1; i < count; i++)
            {
                await _context.Answers.AddAsync(new Answer()
                {
                    Id = ++answersCount,
                    Rating = random.Next(-1, 10),
                    UserId = random.Next(1, 4),
                    QuestionId = questionId,
                    Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                    IsCorrect = false
                });
                await GenerateCommentsToAnswersAsync(answersCount);
            }
        }
        
        private async Task GenerateCommentsToAnswersAsync(int answerId)
        {
            var random = new Random();
            var count = random.Next(1, 6);
            
            for (var i = 1; i < count; i++)
            {
                await _context.Comments.AddAsync(new Comment()
                {
                    UserId = random.Next(1, 4),
                    AnswerId = answerId,
                    Content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                });
            }
        }

        private async Task GenerateUsersAsync()
        {
            await _context.Users.AddAsync(new User()
            {
                Id = 1,
                Name = "Ivan",
                Surname = "Klimov",
                Nickname = "webber",
                Photo = _base64ImagePrefix + GetBase64StringForImage($"{Directory.GetCurrentDirectory()}\\Assets\\1.jpg")
            });
            await _context.Users.AddAsync(new User()
            {
                Id = 2,
                Name = "Natalie",
                Surname = "Portman",
                Nickname = "portmane",
                Photo = _base64ImagePrefix + GetBase64StringForImage($"{Directory.GetCurrentDirectory()}\\Assets\\2.jpg")
            });
            await _context.Users.AddAsync(new User()
            {
                Id = 3,
                Name = "John",
                Surname = "Travolta",
                Nickname = "vincent-vega",
                Photo = _base64ImagePrefix + GetBase64StringForImage($"{Directory.GetCurrentDirectory()}\\Assets\\3.jpg")
            });
        }
        
        private static string GetBase64StringForImage(string imgPath)  
        {  
            byte[] imageBytes = System.IO.File.ReadAllBytes(imgPath);  
            string base64String = Convert.ToBase64String(imageBytes);  
            return base64String;  
        }
    }
}