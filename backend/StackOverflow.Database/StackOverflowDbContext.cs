﻿using Microsoft.EntityFrameworkCore;
using StackOverflow.Database.Models;

namespace StackOverflow.Database
{
    public class StackOverflowDbContext : DbContext
    {
        public StackOverflowDbContext(DbContextOptions<StackOverflowDbContext> options) : base(options) { }
        
        public DbSet<User> Users { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("stack-overflow-db");

            // primary keys
            modelBuilder.Entity<User>().HasKey(u => u.Id);
            modelBuilder.Entity<Question>().HasKey(q => q.Id);
            modelBuilder.Entity<Answer>().HasKey(a => a.Id);
            modelBuilder.Entity<Comment>().HasKey(c => c.Id);

            // one-to-many
            modelBuilder.Entity<User>()
                .HasMany(u => u.Questions)
                .WithOne((q => q.User));
            
            modelBuilder.Entity<User>()
                .HasMany(u => u.Answers)
                .WithOne((q => q.User));
            
            modelBuilder.Entity<User>()
                .HasMany(u => u.Comments)
                .WithOne((q => q.User));
            
            modelBuilder.Entity<Question>()
                .HasMany(u => u.Comments)
                .WithOne((q => q.Question));
            
            modelBuilder.Entity<Answer>()
                .HasMany(u => u.Comments)
                .WithOne((q => q.Answer));
        }

    }
}