﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Core.Models.StackOverflow.Dto
{
    [JsonObject]
    public class BaseEntityDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }

        [JsonProperty("user")]
        public UserDto User { get; set; }

        [JsonProperty("rating")] 
        public int Rating { get; set; }

        [JsonProperty("comments")] 
        public List<CommentDto> Comments { get; set; }
    }
}