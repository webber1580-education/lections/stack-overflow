﻿using Newtonsoft.Json;

namespace Core.Models.StackOverflow.Dto
{
    [JsonObject]
    public class UserDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("surname")]
        public string Surname { get; set; }
        
        [JsonProperty("nickname")]
        public string Nickname { get; set; }
        
        [JsonProperty("photo")]
        public string Photo { get; set; }
    }
}