﻿using Newtonsoft.Json;

namespace Core.Models.StackOverflow.Dto
{
    [JsonObject]
    public class CommentDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("user")]
        public UserDto User { get; set; }

        [JsonProperty("content")]
        public string Content { get; set; }
    }
}