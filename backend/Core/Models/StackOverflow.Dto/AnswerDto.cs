﻿using Newtonsoft.Json;

namespace Core.Models.StackOverflow.Dto
{
    [JsonObject]
    public class AnswerDto : BaseEntityDto
    {
        [JsonProperty("isCorrect")]
        public bool IsCorrect { get; set; }
        
        [JsonProperty("questionId")]
        public int QuestionId { get; set; }
    }
}