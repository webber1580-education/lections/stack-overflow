﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Core.Models.StackOverflow.Dto
{
    [JsonObject]
    public class QuestionDto : BaseEntityDto
    {
        [JsonProperty("answers")]
        public List<AnswerDto> Answers { get; set; }

        [JsonProperty("tags")]
        public string[] Tags { get; set; }
        
        [JsonProperty("theme")]
        public string Theme { get; set; }
    }
}