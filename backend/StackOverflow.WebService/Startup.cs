using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using StackOverflow.Database;
using StackOverflow.Database.Models;
using StackOverflow.Database.Repositories.AnswerRepository;
using StackOverflow.Database.Repositories.CommentRepository;
using StackOverflow.Database.Repositories.GeneratorRepository;
using StackOverflow.Database.Repositories.QuestionRepository;

namespace StackOverflow.WebService
{
    public class Startup
    {
        private readonly string _corsPolicy = "AskBmstuFm";
        
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("StackOverflow");
            services.AddEntityFrameworkNpgsql().AddDbContext<StackOverflowDbContext>(options => 
                options.UseNpgsql(connectionString, x => x.MigrationsAssembly("StackOverflow.WebService"))
            );

            services.AddTransient<IGeneratorRepository, GeneratorRepository>();
            services.AddTransient<ICommentRepository, CommentRepository>();
            services.AddTransient<IAnswerRepository, AnswerRepository>();
            services.AddTransient<IQuestionRepository, QuestionRepository>();
            
            services.AddControllers();
            services.AddHealthChecks();
            services.AddCors(o => o.AddPolicy(_corsPolicy, builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            UpdateDatabase(app);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(_corsPolicy);
            app.UseStaticFiles();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
                endpoints.MapControllers();
            });
        }
        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            using var context = serviceScope.ServiceProvider.GetService<StackOverflowDbContext>();
            context.Database.Migrate();
        }
    }
}