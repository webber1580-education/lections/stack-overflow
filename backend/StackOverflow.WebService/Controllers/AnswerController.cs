﻿using System;
using System.Threading.Tasks;
using Core.Models.StackOverflow.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StackOverflow.Database.Repositories.AnswerRepository;

namespace StackOverflow.WebService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AnswerController : Controller
    {
        private readonly IAnswerRepository _answerRepository;
        
        public AnswerController(IAnswerRepository answerRepository)
        {
            _answerRepository = answerRepository;
        }
        
        [HttpGet("{answerId}")]
        public async Task<IActionResult> GetAnswer(int answerId)
        {
            IActionResult response;
            try
            {
                var answerDto = await _answerRepository.GetAnswerAsync(answerId);
                response = Ok(answerDto);
            }
            catch(Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError); 
            }

            return response;
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateAnswer([FromBody] AnswerDto answerDto)
        {
            IActionResult response;

            try
            {
                await _answerRepository.CreateAnswerAsync(answerDto);
                response = StatusCode(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError);
            }

            return response;
        }
        
        [HttpPut("{answerId}")]
        public async Task<IActionResult> UpdateAnswer(int answerId, [FromBody] AnswerDto answerDto)
        {
            IActionResult response;

            try
            {
                await _answerRepository.UpdateAnswerAsync(answerId, answerDto);
                response = StatusCode(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError);
            }

            return response;
        }
    }
}