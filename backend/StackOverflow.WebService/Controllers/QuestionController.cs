﻿using System;
using System.Threading.Tasks;
using Core.Models.StackOverflow.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StackOverflow.Database.Repositories.QuestionRepository;

namespace StackOverflow.WebService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QuestionController : Controller
    {
        private readonly IQuestionRepository _questionRepository;
        
        public QuestionController(IQuestionRepository questionRepository)
        {
            _questionRepository = questionRepository;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetQuestions()
        {
            IActionResult response;
            try
            {
                var questionListDto = await _questionRepository.GetQuestionsAsync();
                response = Ok(questionListDto);
            }
            catch(Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError); 
            }

            return response;
        }
        
        [HttpGet("{questionId}")]
        public async Task<IActionResult> GetQuestion(int questionId)
        {
            IActionResult response;
            try
            {
                var questionDto = await _questionRepository.GetQuestionAsync(questionId);
                response = Ok(questionDto);
            }
            catch(Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError); 
            }

            return response;
        }
        
        [HttpPost]
        public async Task<IActionResult> CreateQuestion([FromBody] QuestionDto questionDto)
        {
            IActionResult response;
            try
            {
                await _questionRepository.CreateQuestionAsync(questionDto);
                response = StatusCode(StatusCodes.Status201Created);
            }
            catch(Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError); 
            }

            return response;
        }

        [HttpPut("{questionId}")]
        public async Task<IActionResult> UpdateQuestion(int questionId, [FromBody] QuestionDto questionDto)
        {
            IActionResult response;

            try
            {
                await _questionRepository.UpdateQuestionAsync(questionId, questionDto);
                response = StatusCode(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError);
            }

            return response;
        }
        
        [HttpDelete("{questionId}")]
        public async Task<IActionResult> DeleteQuestion(int questionId)
        {
            IActionResult response;

            try
            {
                await _questionRepository.DeleteQuestionAsync(questionId);
                response = StatusCode(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError);
            }

            return response;
        }
    }
}