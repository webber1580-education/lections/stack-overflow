﻿using System;
using System.Threading.Tasks;
using Core.Models.StackOverflow.Dto;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StackOverflow.Database.Repositories.CommentRepository;

namespace StackOverflow.WebService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CommentController : Controller
    {
        private readonly ICommentRepository _commentRepository;
        
        public CommentController(ICommentRepository commentRepository)
        {
            _commentRepository = commentRepository;
        }
        
        [HttpPost("Question/{questionId}")]
        public async Task<IActionResult> CreateQuestionComment(int questionId, [FromBody] CommentDto commentDto)
        {
            IActionResult response;

            try
            {
                await _commentRepository.CreateQuestionCommentAsync(questionId, commentDto);
                response = StatusCode(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError);
            }

            return response;
        }
        
        [HttpPost("Answer/{answerId}")]
        public async Task<IActionResult> CreateAnswerComment(int answerId, [FromBody] CommentDto commentDto)
        {
            IActionResult response;

            try
            {
                await _commentRepository.CreateAnswerCommentAsync(answerId, commentDto);
                response = StatusCode(StatusCodes.Status204NoContent);
            }
            catch (Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError);
            }

            return response;
        }
    }
}