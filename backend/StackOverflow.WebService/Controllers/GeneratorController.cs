﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StackOverflow.Database.Repositories.GeneratorRepository;

namespace StackOverflow.WebService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class GeneratorController : Controller
    {
        private readonly IGeneratorRepository _generatorRepository;
        
        public GeneratorController(IGeneratorRepository generatorRepository)
        {
            _generatorRepository = generatorRepository;
        }
         
        [HttpGet]
        public async Task<IActionResult> GetGenerate()
        {
            IActionResult response;
            try
            {
                await _generatorRepository.GenerateDataAsync();
                response = Ok();
            }
            catch (Exception ex)
            {
                response = StatusCode(StatusCodes.Status500InternalServerError);
            }

            return response;
        }
    }
}