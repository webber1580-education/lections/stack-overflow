import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Answer} from '../models/dto/answer-dto.model';
import {Question} from "../models/dto/question-dto.model";

@Injectable()
export class AnswerService {

    private url = `${environment.baseUrl}/api/Answer`;

    constructor(private http: HttpClient) { }

    public getAnswer(id: number): Observable<Answer> {
        return this.http.get<Answer>(`${this.url}/${id}`);
    }

    public createAnswer(answer: Answer): Observable<void>  {
        return this.http.post<void>(`${this.url}`, answer);
    }

    public updateAnswer(answerId: number, answer: Answer): Observable<void> {
        return this.http.put<void>(`${this.url}/${answerId}`, answer);
    }
}
