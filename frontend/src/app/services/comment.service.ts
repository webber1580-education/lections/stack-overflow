import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable()
export class CommentService {

    private url = `${environment.baseUrl}/api/Comment`;

    constructor(private http: HttpClient) { }

    public createQuestionComment(questionId: number, comment: string): Observable<void>  {
        const source = { content: comment };
        return this.http.post<void>(`${this.url}/Question/${questionId}`, source);
    }

    public createAnswerComment(answerId: number, comment: string): Observable<void>  {
        const source = { content: comment };
        return this.http.post<void>(`${this.url}/Answer/${answerId}`, source);
    }
}
