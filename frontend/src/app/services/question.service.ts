import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {Question} from '../models/dto/question-dto.model';

@Injectable()
export class QuestionService {

    private url = `${environment.baseUrl}/api/Question`;

    constructor(private http: HttpClient) { }

    public getQuestions(): Observable<Question[]> {
        return this.http.get<Question[]>(`${this.url}`);
    }

    public getQuestion(id: number): Observable<Question> {
        return this.http.get<Question>(`${this.url}/${id}`);
    }

    public createQuestion(question: Question): Observable<void> {
        return this.http.post<void>(this.url, question);
    }

    public updateQuestion(id: number, question: Question): Observable<void> {
        return this.http.put<void>(`${this.url}/${id}`, question);
    }

    public deleteQuestion(id: number): Observable<void> {
        return this.http.delete<void>(`${this.url}/${id}`);
    }
}
