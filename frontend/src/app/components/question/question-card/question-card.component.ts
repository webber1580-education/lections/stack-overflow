import {Component, Input, OnInit} from '@angular/core';
import {QuestionService} from '../../../services/question.service';
import {Question} from '../../../models/dto/question-dto.model';
import {CommentService} from "../../../services/comment.service";

@Component({
    selector: 'app-question-card',
    templateUrl: './question-card.component.html',
    styleUrls: ['./question-card.component.scss']
})
export class QuestionCardComponent {

    @Input() public question: Question;

    constructor(private questionService: QuestionService, private commentService: CommentService) { }

    onUpdateQuestionRating(inc: number) {
        const updatedQuestion = {...this.question};
        updatedQuestion.rating += inc;
        this.questionService.updateQuestion(this.question.id, updatedQuestion).subscribe(
            () => this.question = updatedQuestion,
            (err) => console.error('Error: ', err)
        );
    }

    onAddQuestionComment(comment: string) {
        this.commentService.createQuestionComment(this.question.id, comment).subscribe(
            () => this.questionService.getQuestion(this.question.id).subscribe(
                question => this.question = question,
                err => console.error('Error: ', err)
            ),
            err => console.error('Error: ', err)
        );
    }
}
