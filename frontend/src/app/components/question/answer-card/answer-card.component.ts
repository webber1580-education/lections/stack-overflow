import {Component, Input, OnInit} from '@angular/core';
import {QuestionService} from '../../../services/question.service';
import {Answer} from '../../../models/dto/answer-dto.model';
import {AnswerService} from "../../../services/answer.service";
import {CommentService} from "../../../services/comment.service";

@Component({
  selector: 'app-answer-card',
  templateUrl: './answer-card.component.html',
  styleUrls: ['./answer-card.component.scss']
})
export class AnswerCardComponent {

    @Input() public answer: Answer;

    constructor(private questionService: QuestionService,
                private answerService: AnswerService,
                private commentService: CommentService) { }

    onUpdateAnswerRating(inc: number) {
        const updatedAnswer = {...this.answer};
        updatedAnswer.rating += inc;
        this.answerService.updateAnswer(this.answer.id, updatedAnswer).subscribe(
            () => this.answer = updatedAnswer,
            (err) => console.error('Error: ', err)
        );
    }

    onRightAnswerSelected() {
        const updatedAnswer = {...this.answer};
        updatedAnswer.isCorrect = true;
        this.answerService.updateAnswer(this.answer.id, updatedAnswer).subscribe(
            () => this.answer = updatedAnswer,
            (err) => console.error('Error: ', err)
        );
    }

    onAddAnswerComment(comment: string) {
        this.commentService.createAnswerComment(this.answer.id, comment).subscribe(
            () => this.answerService.getAnswer(this.answer.id).subscribe(
                () => this.answerService.getAnswer(this.answer.id).subscribe(
                    answer => this.answer = answer,
                    err => console.error('Error: ', err)
                ),
                err => console.error('Error: ', err)
            ),
            err => console.error('Error: ', err)
        );
    }
}
